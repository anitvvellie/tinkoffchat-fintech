//
//  ViewController.swift
//  TinkoffChat
//
//  Created by Alevtina on 23/09/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("👶🏻 View is about to appear: \(#function) 👶🏻 \n")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("🙇‍♀️ View did appear: \(#function) 🙇‍♀️ \n")
    }
    
    override func viewWillLayoutSubviews() {
        print("🤰 View is about to layout its subviews: \(#function) 🤰 \n")
    }
    
    override func viewDidLayoutSubviews() {
        print("🤱🏼 View did just laid out its subviews: \(#function) 🤱🏼 \n")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("👴🏻 View is about to be removed from a view hierarchy: \(#function) 👴🏻 \n")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("☠️ View was removed from a view hierarchy: \(#function) ☠️ \n")
    }

}

